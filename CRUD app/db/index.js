var mysql = require('mysql');
var pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'chirps'
});

let db = {};
db.selectAll = () => {
        return new Promise((resolve, reject) =>{
            pool.query('SELECT * FROM users', (err, results) =>{
                if(err){
                    return reject(err);
                }
                return resolve(results);
            });
        });
    }
    db.getOne = (id) => {
        return new Promise((resolve, reject) =>{
            pool.query('SELECT * FROM users WHERE id = ?',[id], (err, results) =>{
                if(err){
                    return reject(err);
                }
                return resolve(results[0]);
            });
        });
    }
    db.deleteAll = () => {
        return new Promise((resolve, reject) =>{
            pool.query('DELETE FROM users', (err, results) =>{
                if(err){
                    return reject(err);
                }
                return resolve(results);
            });
        });
    }
    db.selectTitle = (title) => {
        return new Promise((resolve, reject) =>{
            pool.query('SELECT * FROM users WHERE TITLE LIKE %?%', [title], (err, results) =>{
                if(err){
                    return reject(err);
                }
                return resolve(results[0]);
            });
        });
    }


module.exports = db;
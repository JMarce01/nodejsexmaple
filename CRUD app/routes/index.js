const express = require('express');
const database = require('../db');
const router = express.Router();



router.get('/', async (req, res, next)=>{
    try {
        let results = await database.selectAll();
        res.json(results);

    }catch(e){
        console.log(e);
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res, next)=>{
    try {
        let results = await database.getOne(req.params.id);
        res.json(results);

    }catch(e){
        console.log(e);
        res.sendStatus(500);
    }
});

router.delete('/', async (req, res, next)=>{
    try {
        let results = await database.deleteAll();
        res.json(results);

    }catch(e){
        console.log(e);
        res.sendStatus(500);
    }
});
module.exports = router;